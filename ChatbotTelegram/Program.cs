﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.InputFiles;

namespace ChatbotTelegram
{
    class Program
    {
        static TelegramBotClient Bot = new TelegramBotClient("2103579675:AAHr2QKbRfUINOQ-ffNU4GOIzYxL1PKeRaY");
        static string hinhanh = @"E:\Images\image\";
        static List<string> anh = new List<string>
        {
            "80802039_2438580193137410_5151150257450516480_n.jpg",
            "earth_sun_planet_surface_stars_99478_1920x1080.jpg",
            "Yêu Yêu.jpg",
            "Thầy ba vã nhẹ.mp4"

        };

        static void Main(string[] args)
        {
            Bot.StartReceiving();
            Bot.OnMessage += Bot_OnMessage;
            Console.ReadLine();
        }

        private static async void Bot_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {

            var text = e.Message.Text;
            switch (text)
            {
                case "Ngày giờ hôm nay":
                    var ngay = DateTime.Now.ToString();
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, ngay);
                    break;
                case "Tên của bạn là gì":
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Telegram Bot");
                    break;
                case "Hello":
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Hello " + e.Message.Chat.Username);
                    break;
                case "mèo":
                    using (var stream = File.OpenRead(hinhanh + anh[0]))
                    {
                        InputOnlineFile nhapanh = new InputOnlineFile(stream);
                        await Bot.SendPhotoAsync(e.Message.Chat.Id, nhapanh);
                        Bot.SendTextMessageAsync(e.Message.Chat.Id, "Đây là mèo của tôi");
                    }
                    break;
                case "video":
                    using (var stream = File.OpenRead(hinhanh + anh[3]))
                    {
                        InputOnlineFile nhapanh = new InputOnlineFile(stream);
                        await Bot.SendVideoAsync(e.Message.Chat.Id, nhapanh);
                     
                    }
                    break;
                default:
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Xin lỗi tôi không hiểu");
                    break;

            }

        }
    }
}
    

